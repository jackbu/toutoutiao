role :app, %w{deploy_user@18.188.176.183}
role :web, %w{deploy_user@18.188.176.183}
role :db,  %w{deploy_user@18.188.176.183}
set :ssh_options, {
   keys: %w(~/Documents/GitHub/deploy_user.pem),
   forward_agent: false,
   auth_methods: %w(publickey password)
 }
