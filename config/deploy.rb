set :application, "toutoutiao"
set :repo_url, "git@gitlab.com:jackbu/toutoutiao.git"
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads')
set :rvm_ruby_version, '2.5.3'
set :passenger_restart_with_touch, true